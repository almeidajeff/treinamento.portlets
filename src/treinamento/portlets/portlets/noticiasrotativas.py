# -*- coding: utf-8 -*-
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from treinamento.portlets import _
from plone import api
from plone.app.portlets.portlets import base
from plone.portlets.interfaces import IPortletDataProvider
from zope.interface import implementer
from z3c.form import form
from zope import schema



class INoticiasRotativasPortlet(IPortletDataProvider):
    """Portlet interface based on  IPortletDataProvider """

    header = schema.TextLine(title=_(u"Header field"),
                             description=_(u"A field to use"),
                             required=True)

@implementer(INoticiasRotativasPortlet)
class Assignment(base.Assignment):
    """ Define fields """
    header = u""

    def __init__(self,header=u""):
        self.header = header

    @property
    def portlet_id(self):
        """Return the portlet assignment's unique object id.
        """
        return id(self)

    @property
    def title(self):
        if self.header:
            return self.header
        else:
            return _(u'Noticias rotativas')

    def get_banners(self):
        L = []
        banners = api.content.find(portal_type="News Item",
                                   sort_on="getObjPositionInParent",
                    )
        if banners:
            for banner in banners:
                D={}
                obj = banner.getObject()
                D['titulo'] = obj.Title()
                D['descricao'] = obj.Description()[:150] + '...'
                D['link'] = obj.absolute_url()
                D['data-criacao'] = obj.effective_date.strftime('%d/%m/%Y')
                L.append(D)
        return L


class Renderer(base.Renderer):
    render = ViewPageTemplateFile('noticiasrotativas.pt')


class AddForm(base.AddForm):

    schema = INoticiasRotativasPortlet
    label = _(u"Adicionar Portlet Noticias Rotativas")
    description = _(
        u"Este portlet deve apresentar as notícias publicadas no portal"
    )
    form_fields = form.field.Fields(INoticiasRotativasPortlet)

    def create(self, data):
        return Assignment(**data)


class EditForm(base.EditForm):

    schema = INoticiasRotativasPortlet
    label = _(u"Editar Portlet Noticias Rotativas")
    description = _(
        u"Este portlet deve apresentar as notícias publicadas no portal"
    )

    form_fields = form.field.Fields(INoticiasRotativasPortlet)
